"""
Scraping posts from the Facebook page.
TODO: Function post stuff to Music Hunters will be added here
"""
import settings

import facebook
import json


def get_posts() -> list:
    if settings.debug:
        with open("cache.json", "r") as file:
            posts = json.load(file)
    else:
        graph = facebook.GraphAPI(access_token=settings.services_keys['FACEBOOK_ACCESS_TOKEN'], version="3.1")
        graph_path = f"/{settings.services_keys['FACEBOOK_PAGE_ID']}/feed?fields=created_time,attachments{{unshimmed_url}}&limit=100"

        posts = graph.get_object(graph_path)
        if settings.save:
            with open("cache.json", "w") as file:
                json.dump(posts, file)
                print("New cache saved, exiting...")
                exit(0)

    return posts['data']
