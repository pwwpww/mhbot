from pprint import pprint

import fb
import filters
import services
import settings


def main():
    # Retrieve posts
    posts = list(filter(filters.dates, fb.get_posts()))
    if settings.debug:
        print("Last week's posts retrieved : {}".format(len(posts)))

    # Get URLs from posts
    urls = list(map(filters.post_urls, posts))

    # Sort URLs by service provider and convert urls -> resource_ids
    resource_ids = dict()
    for service_name in settings.services_import:
        if urls:
            filter_method = getattr(filters, service_name)
            resource_ids[service_name], urls = filter_method(urls)

            if settings.debug:
                print("--- {} filter ---".format(service_name.capitalize()))
                print("Links found : {}".format(len(resource_ids[service_name])))
                print("Links remaining : {}".format(len(urls)))

    # Create a playlist for every service
    for service_export in settings.services_export:
        # Create a list of resource_ids suited for each service_export
        export_resource_ids = resource_ids[service_export].copy()
        for service_import in settings.services_import:
            if service_import is not service_export:
                export_resource_ids += services.convert_resource_ids(service_import,
                                                                     service_export,
                                                                     resource_ids[service_import])

        pprint(export_resource_ids)
        print(services.create_playlist(service_export, export_resource_ids))


if __name__ == "__main__":
    main()
