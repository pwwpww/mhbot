"""
Functions used to operate on Facebook posts and urls
"""
import re
from datetime import datetime

import settings


# Return post in specified timeframe
def dates(post: dict):
    date = settings.dates
    post_date = datetime.strptime(post['created_time'], '%Y-%m-%dT%H:%M:%S+0000')
    if date['week_start'] < post_date < date['week_end']:
        return True


# TODO: make this also traverse post's comments - will probably need to get rid of a filter call to this
def post_urls(post: dict):
    return post['attachments']['data'][0]['unshimmed_url']


# TODO Youtube and Spotify are nearly identical and can be merged
def youtube(urls: list):
    """Returns 2 lists - Youtube resource ids and non-Youtube urls"""
    youtube_ids = []
    other_urls = []
    # TODO move re pattern to a setting
    # pattern from https://gist.github.com/brunodles/927fd8feaaccdbb9d02b
    pattern = re.compile(
        "(?:https?://)?(?:www\.)?youtu\.?be(?:\.com)?/?.*(?:watch|embed)?(?:.*v=|v/|/)([\w\-_]+)&?")

    for url in urls:
        result = pattern.search(url)
        if result:
            youtube_ids.append(result.group(1))
        else:
            other_urls.append(url)

    return youtube_ids, other_urls


def spotify(urls: list):
    """Returns 2 lists - Spotify resource ids and non-Spotify urls"""
    spotify_ids = []
    other_urls = []
    # TODO move re pattern to a setting
    # pattern from https://gist.github.com/fantattitude/3627354
    pattern = re.compile(
        "https?://open.spotify.com/track/([a-zA-Z0-9]+)")

    for url in urls:
        result = pattern.search(url)
        if result:
            spotify_ids.append(result.group(1))
        else:
            other_urls.append(url)

    return spotify_ids, other_urls
