"""
Functions used to interact with Youtube's API
"""

import os
import settings

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors


def search(search_query: str) -> list:
    scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]

    # Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
    # tab of
    #   https://cloud.google.com/console
    # Please ensure that you have enabled the YouTube Data API for your project.

    youtube = googleapiclient.discovery.build('youtube', 'v3',
                                              developerKey=settings.services_keys['YOUTUBE_DEVELOPER_KEY'])

    search_response = youtube.search().list(
        part="id",
        maxResults=1,
        q=search_query,
        safeSearch="none",
        type="video",
    ).execute()

    return search_response


def search_resource_id():
    pass


def create_playlist(playlist_title: str, playlist_description: str, export_resource_ids: list) -> str:
    scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    # Authorisation
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_config(
        settings.services_keys["YOUTUBE_CLIENT_CONFIG"], scopes)
    credentials = flow.run_console()
    youtube = googleapiclient.discovery.build('youtube', 'v3', credentials=credentials)

    # Create playlist
    request = youtube.playlists().insert(
        part="snippet,status",
        body={
            "snippet": {
                "title": playlist_title,
                "description": playlist_description,
                "defaultLanguage": "en",
            },
            "status": {
                "privacyStatus": "unlisted"
            }
        }
    )
    response = request.execute()

    playlist_id = response['id']

    # Populate playlist
    for video_id in export_resource_ids:
        request = youtube.playlistItems().insert(
            part="snippet",
            body={
                "snippet": {
                    "playlistId": playlist_id,
                    "position": 0,
                    "resourceId": {
                        "kind": "youtube#video",
                        "videoId": video_id,
                    }
                }
            }
        )
        response = request.execute()

    return playlist_id
