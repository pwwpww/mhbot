"""
Functions used to interact with Spotify's API
"""

import settings

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials


def auth():
    return SpotifyClientCredentials(client_id=settings.services_keys['SPOTIFY_CLIENT_ID'],
                                    client_secret=settings.services_keys['SPOTIFY_CLIENT_SECRET'])


def search():
    pass


def search_resource_id(resource_id: str) -> dict:
    spotify = spotipy.Spotify(client_credentials_manager=auth())
    return spotify.track(resource_id)
