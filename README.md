# mhbot

A little script used to help out with [Music Hunters](https://www.facebook.com/groups/1665296607026713/).
Used to sum up last week's activity on the group by creating playlists which include music from all page posts from the previous week.

#### How it works/TODO

1. Get posts from the page **DONE**, see Notes
2. Filter posts by dates **DONE**
3. Get URLs from posts **DONE**
4. Extract resource IDs from URLs and sort by service provider **DONE**
    1. Youtube **DONE**
    2. Spotify **DONE**
5. Create playlists **TODO**
    1. Create Youtube playlist **DONE**
        1. Add Youtube links **DONE**
        2. Add Spotify links **DONE**
            1. Convert Spotify -> Youtube **DONE**
    2. Create Spotify playlist **TODO**
        1. Add Spotify links **WOULD SORT OF WORK ALREADY**
        2. Add Youtube links 
            1. Convert Youtube -> Spotify
6. Create a post on Music Hunters page **TODO**

#### Notes

1. Facebook group feed API's `since`/`until` parameters apply to the `updated_time` field
    - I initially assumed it works on `created_time` field
    - Relying on those params could result in a slightly inconsistent behaviour (certain songs appearing in multiple playlists)
    - Instead, we need to grab more posts, then filter them programmatically
  
### Running

- `virtualenv .env && source .env/bin/activate && pip install -r requirements.txt`
- `cp services_keys_default.json services_keys.json`
    - `services_keys.json` is supposed to hold all keys needed for API interaction - populate as neccessary if you're not planning on using a particular service
    - Info on what those keys are used for and how to get obtain them are listed below: 
        - `FACEBOOK_ACCESS_TOKEN` - needed to grab posts from MH page
            - ask me and I'll add you to the FB app project so you can generate on for yourself
        - `FACEBOOK_PAGE_ID` - no need to change this one!
        - `SPOTIFY_CLIENT_ID` and `SPOTIFY_CLIENT_SECRET` - needed for all Spotify stuff
            - https://developer.spotify.com/dashboard/
            - `Create a client ID`
            - copy and paste generated client ID and client secret
        - `YOUTUBE_DEVELOPER_KEY` - needed to search Youtube for tracks
            - https://console.developers.google.com/apis/credentials
            - `Create credentials`
            - `API key`
            - copy and paste generated key
        - `YOUTUBE_CLIENT_SECRET` - needed to create Youtube playlists
            - https://console.developers.google.com/apis/credentials
            - `Create credentials`
            - `OAuth client ID`
            - copy and paste contents of generated .json file as value for that key
            
- `python3 main.py`