"""
Parse command-line arguments, load services' secrets from file.
"""
import argparse
import json
from datetime import datetime, timedelta, timezone

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--debug',
                    action='store_true',
                    dest='debug',
                    help='Use cached Facebook posts; super verbose')
parser.add_argument('-s', '--save',
                    action='store_true',
                    dest='save',
                    help='Save FB posts into cache.json, then quit')
parser.add_argument('--date',
                    action='store',
                    dest='date',
                    help='Specify a different date in YYYY-MM-DD format - will then get posts for that week. Useful '
                         'for testing historical cache files.')
params = parser.parse_args()


def get_datetime() -> dict:

    if date:
        postdate = datetime.fromisoformat(date)
    else:
        postdate = datetime.now(timezone.utc).replace(microsecond=0) - timedelta(weeks=1)   # datetime.utcnow() was returning local tz

    week_start = postdate.replace(hour=0, minute=0, second=0) - timedelta(postdate.weekday())
    week_end = week_start + timedelta(weeks=1, seconds=-1)

    if debug:
        if not date:
            print("Local datetime     : {}".format(datetime.now()))
            print("UTC datetime       : {}".format(datetime.now(timezone.utc)))
        print("Posts from week of : {}".format(postdate))
        print("Week start         : {}".format(week_start))
        print("Week end           : {}".format(week_end))

    return {'week_start': week_start, 'week_end': week_end}


debug = params.debug
save = params.save
date = params.date
dates = get_datetime()

# Services to import URLs from
services_import = ['spotify',
                   'youtube']

# Services to export playlists to
services_export = ['spotify',
                   'youtube']


with open("services_keys.json", "r") as file:
    services_keys = json.load(file)