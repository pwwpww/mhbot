"""
Functions with higher-level logic for interacting with services.
Functions interacting directly with services' APIs are located in *_wrapper.py modules.

resource_id_import_* functions take a resource_id and return a {'artist', 'title'} dict
resource_id_export_* functions take a {'artist', 'title'} dict and return a resource_id
create_playlist_*    functions take a service name and list of resource_ids for that service, return a playlist url
"""
import settings
import spotify_wrapper
import youtube_wrapper


# Convert a list of resource_ids from one service to another
def convert_resource_ids(service_import: str, service_export: str, resource_ids: list) -> list:
    converted_resource_ids = []
    for resource_id in resource_ids:
        import_function = globals()['resource_id_import_' + service_import]
        export_function = globals()['resource_id_export_' + service_export]

        track_name = import_function(resource_id)
        converted_resource_id = export_function(track_name)

        converted_resource_ids.append(converted_resource_id)

    return converted_resource_ids


def create_playlist(service_export: str, export_resource_ids: list) -> str:
    playlist_title = "Music Hunters playlist"
    playlist_description = "Playlist description"
    create_playlist_function = globals()['create_playlist_' + service_export]
    if settings.debug:
        return service_export
    playlist_url = create_playlist_function(playlist_title, playlist_description, export_resource_ids)

    return playlist_url


def resource_id_import_spotify(resource_id: str) -> dict:

    results = spotify_wrapper.search_resource_id(resource_id)

    # Handling tracks with multiple artists
    artists = []
    for artist in results ['artists']:
        artists += [artist['name']]

    artist = ' '.join(artists)
    title = results['name']
    track = {'artist': artist, 'title': title}

    return track


def resource_id_import_youtube(resource_id: str) -> dict:
    pass


def resource_id_export_spotify(track: dict) -> str:
    pass


def resource_id_export_youtube(track: dict) -> str:
    search_query = "{0} - {1}".format(track['artist'], track['title'])
    result = youtube_wrapper.search(search_query)
    resource_id = result['items'][0]['id']['videoId']

    return resource_id


def create_playlist_spotify(playlist_title: str, playlist_description: str, export_resource_ids: list):
    pass


def create_playlist_youtube(playlist_title: str, playlist_description: str, export_resource_ids: list):
    playlist_id = youtube_wrapper.create_playlist(playlist_title, playlist_description, export_resource_ids)
    playlist_url = "https://www.youtube.com/playlist?list={}".format(playlist_id)

    return playlist_url
